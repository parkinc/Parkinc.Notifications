﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Parkinc.Notifications.DAL.Entities;

namespace Parkinc.Notifications.DAL.Access.Interfaces
{
    public interface ISubscriptionAccess
    {
        Task<SubscriptionEntity> InsertAsync(SubscriptionEntity entity);
        Task RemoveAsync(int id);
        Task<IEnumerable<SubscriptionEntity>> GetAllAsync();
        Task<IEnumerable<SubscriptionEntity>> GetWithEmailAsync(string email, string phone);
    }
}
