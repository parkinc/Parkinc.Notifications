﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Options;
using Parkinc.Notifications.DAL.Access.Interfaces;
using Parkinc.Notifications.DAL.Entities;
using Parkinc.Notifications.Models.Configuration;

namespace Parkinc.Notifications.DAL.Access
{
    public class SubscriptionAccess : ISubscriptionAccess
    {
        private readonly IOptions<SqlConfig> _sqlConfig;

        public SubscriptionAccess(IOptions<SqlConfig> sqlConfig)
        {
            _sqlConfig = sqlConfig;
        }

        public async Task<SubscriptionEntity> InsertAsync(SubscriptionEntity entity)
        {
            const string query = @"Insert into dbo.[Subscriptions] values (@ParkingPlaceId, @Email, @Phone, @Hour, @Minute);
                                 SELECT CAST(SCOPE_IDENTITY() as int)";

            using (var con = new SqlConnection(_sqlConfig.Value.ConnectionString))
            {
                try
                {
                    var result = await con.QueryAsync<int>(query, Parameters(entity));
                    entity.Id = result.First();

                    return entity;
                }
                catch
                {
                    throw new Exception("DB ERR");
                }         
            }
        }

        public async Task RemoveAsync(int id)
        {
            const string query = "Delete from dbo.[Subscriptions] where Id = @Id";

            using (var con = new SqlConnection(_sqlConfig.Value.ConnectionString))
            {
                try
                {
                    await con.ExecuteAsync(query, new {Id = id});
                }
                catch
                {
                    throw new Exception("DB ERR");
                }
            }
        }

        public async Task<IEnumerable<SubscriptionEntity>> GetAllAsync()
        {
            const string query = "Select * from dbo.[Subscriptions]";

            using (var con = new SqlConnection(_sqlConfig.Value.ConnectionString))
            {
                try
                {
                    return await con.QueryAsync<SubscriptionEntity>(query);
                }
                catch
                {
                    throw new Exception("DB ERR");
                }
            }

        }

        public async Task<IEnumerable<SubscriptionEntity>> GetWithEmailAsync(string email, string phone)
        {
            const string query = "Select * from [Subscriptions] where Email= @Email or Phone= @Phone";

            using (var con = new SqlConnection(_sqlConfig.Value.ConnectionString))
            {
                try
                {
                    return await con.QueryAsync<SubscriptionEntity>(query, new {Email = email, Phone = phone});
                }
                catch
                {
                    throw new Exception("DB ERR");
                }
            }
        }

        private static object Parameters(SubscriptionEntity entity)
        {
            return new
            {
                entity.Id,
                entity.ParkingPlaceId,
                entity.Email,
                entity.Phone,
                entity.Hour,
                entity.Minute
            };
        }
    }
}
