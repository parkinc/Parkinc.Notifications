﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parkinc.Notifications.DAL.Entities
{
    public class SubscriptionEntity
    {
        public int Id { get; set; }
        public int ParkingPlaceId { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int Hour { get; set; }
        public int Minute { get; set; }

        public DateTime DateTime => new DateTime()
            .AddHours(Hour)
            .AddMinutes(Minute);
    }
}
