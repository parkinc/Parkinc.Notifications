﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Parkinc.Notifications.Models.Configuration
{
    public class SqlConfig
    {
        public string ConnectionString { get; set; }
    }
}
