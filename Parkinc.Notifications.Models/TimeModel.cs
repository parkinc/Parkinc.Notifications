﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Parkinc.Notifications.Models
{
    public class TimeModel
    {
        [Required(ErrorMessage = "Hour is required")]
        public int Hour { get; set; }

        [Required(ErrorMessage = "Minute is required")]
        public int Minute { get; set; }

        public DateTime DateTime => new DateTime()
            .AddHours(Hour)
            .AddMinutes(Minute);

        public bool IsValid
        {
            get
            {
                if (Hour < 0 || Hour > 23) return false;
                if (Minute < 0 || Minute > 59) return false;
                return true;
            }
        }
    }
}
