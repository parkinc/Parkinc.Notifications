﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Parkinc.Notifications.Models
{
    public class SubscriptionModel
    {
        [Range(0, int.MaxValue, ErrorMessage = "Id cannot be negative")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Parking place id is required")]
        public int ParkingPlaceId { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [EmailAddress(ErrorMessage = "Email must be valid")]
        public string Email { get; set; }

        [Phone(ErrorMessage = "Phone must be valid")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Hour is required")]
        [Range(0, 23, ErrorMessage = "Hour must be in 0-23 format")]
        public int Hour { get; set; }

        [Range(0, 59, ErrorMessage = "Minute must be in 0-59 format")]
        public int Minute { get; set; }
    }
}
