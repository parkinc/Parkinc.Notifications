﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Parkinc.Notifications.Core;
using Parkinc.Notifications.Core.Interfaces;
using Parkinc.Notifications.DAL.Access;
using Parkinc.Notifications.DAL.Access.Interfaces;
using Parkinc.Notifications.DAL.Entities;
using Parkinc.Notifications.Models;
using Parkinc.Notifications.Models.Configuration;

namespace Parkinc.Notifications.API
{
    public class Startup
    {
        private readonly IConfiguration _appSettingsConfiguration;
        private readonly IMapper _mapper;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            _appSettingsConfiguration = ConfigureAppSettings();
            _mapper = ConfigureMapper();
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.Configure<SqlConfig>(_appSettingsConfiguration.GetSection("SubscriptionDatabase"));
            services.AddOptions();

            services.AddSingleton(_mapper);

            services.AddSingleton<ISubscriptionCore, SubscriptionCore>();
            services.AddSingleton<ISubscriptionAccess, SubscriptionAccess>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }

        private static IConfiguration ConfigureAppSettings()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true);

            return builder.Build();
        }

        private static IMapper ConfigureMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<SubscriptionModel, SubscriptionEntity>();
            });

            return config.CreateMapper();
        }
    }
}
