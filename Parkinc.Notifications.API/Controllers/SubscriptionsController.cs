﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Parkinc.Notifications.Models;
using Parkinc.Notifications.Core.Interfaces;

namespace Parkinc.Notifications.API.Controllers
{
    [Route("api/[controller]")]
    public class SubscriptionsController : Controller
    {
        private readonly ISubscriptionCore _subscriptionCore;

        public SubscriptionsController(ISubscriptionCore subscriptionCore)
        {
            _subscriptionCore = subscriptionCore;
        }

        // POST api/subscriptions
        [HttpPost]
        public async Task<IActionResult> Create([FromBody]SubscriptionModel model)
        {
            try
            {
                if (ModelState.IsValid == false)
                {
                    return BadRequest();
                }

                var result = await _subscriptionCore.CreateSubscription(model);
                return Created($"api/subscriptions/{result.Id}", result);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        // DELETE api/subscriptions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return BadRequest();
                }

                await _subscriptionCore.RemoveSubscription(id);
                return Ok();
            }
            catch
            {
                return StatusCode(500);
            }
        }

        // GET api/subscriptions/unnotified
        [HttpGet("unnotified")]
        public async Task<IActionResult> GetUnnotifiedSubscriptions(int lastNotifiedHour, int lastNotifiedMinute)
        {
            try
            {
                var model = new TimeModel()
                {
                    Hour = lastNotifiedHour,
                    Minute = lastNotifiedMinute
                };

                if (model.IsValid == false)
                {
                    return BadRequest();
                }

                var result = await _subscriptionCore.GetUnnotifiedSubscriptions(model);
                return Ok(result);
            }
            catch
            {
                return StatusCode(500);
            }
        }

        // GET api/subscriptions
        [HttpGet]
        public async Task<IActionResult> Get(string email, string phone)
        {
            //try
            //{
                var result = await _subscriptionCore.GetSubscripscriptions(email, phone);
                return Ok(result);
            //}
            //catch
            //{
                //return StatusCode(500);
            //}
        }
    }
}
