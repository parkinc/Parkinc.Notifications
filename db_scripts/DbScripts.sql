create database [Parkinc.Notifications]
go
--CREATE
use [Parkinc.Notifications]
go
create table dbo.[Subscriptions](
	Id					int				not null			identity(1,1),
	ParkingPlaceId		int				not null,
	Email				varchar(30)		not null,
	Phone				varchar(40)		null,
	[Hour]				int				not null,
	[Minute]			int				null				default(0),

	primary key(id)
);
--POPULATE
use [Parkinc.Notifications]
Insert into dbo.[Subscriptions] values (100, 'miropakanec@gmail.com', '+421910245649', 8,0);
Insert into dbo.[Subscriptions] values (101, 'miropakanec@gmail.com', '+421910245649', 9,0);
Insert into dbo.[Subscriptions] values (102, 'miropakanec@gmail.com', '+421910245649', 10,0);
Insert into dbo.[Subscriptions] values (103, 'miropakanec@gmail.com', '+421910245649', 11,0);
Insert into dbo.[Subscriptions] values (104, 'miropakanec@gmail.com', '+421910245649', 12,0);
Insert into dbo.[Subscriptions] values (105, 'miropakanec@gmail.com', '+421910245649', 13,0);
Insert into dbo.[Subscriptions] values (106, 'miropakanec@gmail.com', '+421910245649', 14,0);
Insert into dbo.[Subscriptions] values (107, 'miropakanec@gmail.com', '+421910245649', 15,0);
Insert into dbo.[Subscriptions] values (108, 'miropakanec@gmail.com', '+421910245649', 16,0);
Insert into dbo.[Subscriptions] values (109, 'miropakanec@gmail.com', '+421910245649', 17,0);
Insert into dbo.[Subscriptions] values (110, 'miropakanec@gmail.com', '+421910245649', 18,0);

Insert into dbo.[Subscriptions] values (100, 'valer1212@gmail.com', '+45666999', 8,30);
Insert into dbo.[Subscriptions] values (101, 'valer1212@gmail.com', '+45666999', 9,30);
Insert into dbo.[Subscriptions] values (102, 'valer1212@gmail.com', '+45666999', 10,30);
Insert into dbo.[Subscriptions] values (103, 'valer1212@gmail.com', '+45666999', 11,30);
Insert into dbo.[Subscriptions] values (104, 'valer1212@gmail.com', '+45666999', 12,30);
Insert into dbo.[Subscriptions] values (105, 'valer1212@gmail.com', '+45666999', 13,30);
Insert into dbo.[Subscriptions] values (106, 'valer1212@gmail.com', '+45666999', 14,30);
Insert into dbo.[Subscriptions] values (107, 'valer1212@gmail.com', '+45666999', 15,30);
Insert into dbo.[Subscriptions] values (108, 'valer1212@gmail.com', '+45666999', 16,30);
Insert into dbo.[Subscriptions] values (109, 'valer1212@gmail.com', '+45666999', 17,30);
Insert into dbo.[Subscriptions] values (110, 'valer1212@gmail.com', '+45666999', 18,30);

--SELECT
use [Parkinc.Notifications]
select * from dbo.[Subscriptions]

--DROP
use [Parkinc.Notifications]
go
drop table dbo.[Subscriptions]

Select * from dbo.[Subscriptions] where Email= '' or Phone= '+421910245649'