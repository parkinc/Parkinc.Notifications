﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Parkinc.Notifications.Models;

namespace Parkinc.Notifications.Core.Interfaces
{
    public interface ISubscriptionCore
    {
        Task<SubscriptionModel> CreateSubscription(SubscriptionModel model);
        Task RemoveSubscription(int id);
        Task<IEnumerable<SubscriptionModel>> GetUnnotifiedSubscriptions(TimeModel lastNotifiedAt);
        Task<IEnumerable<SubscriptionModel>> GetSubscripscriptions(string email, string phone);
    }
}
