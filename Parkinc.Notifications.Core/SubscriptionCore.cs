﻿using Parkinc.Notifications.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Parkinc.Notifications.Core.Interfaces;
using Parkinc.Notifications.DAL.Access.Interfaces;
using Parkinc.Notifications.DAL.Entities;

namespace Parkinc.Notifications.Core
{
    public class SubscriptionCore : ISubscriptionCore
    {
        private readonly ISubscriptionAccess _subscriptionAccess;
        private readonly IMapper _mapper;

        public SubscriptionCore(ISubscriptionAccess subscriptionAccess, IMapper mapper)
        {
            _subscriptionAccess = subscriptionAccess;
            _mapper = mapper;
        }

        public async Task<SubscriptionModel> CreateSubscription(SubscriptionModel model)
        {
            var entity = _mapper.Map<SubscriptionEntity>(model);
            var result = await _subscriptionAccess.InsertAsync(entity);
            return _mapper.Map<SubscriptionModel>(result);
        }

        public async Task RemoveSubscription(int id)
        {
            await _subscriptionAccess.RemoveAsync(id);
        }

        public async Task<IEnumerable<SubscriptionModel>> GetUnnotifiedSubscriptions(TimeModel lastNotifiedAt)
        {
            var entities = await _subscriptionAccess.GetAllAsync();
            var timeMark = new DateTime()
                .AddHours(DateTime.Now.Hour)
                .AddHours(1)
                .AddMinutes(DateTime.Now.Minute).ToUniversalTime();

            /*var unnotified = entities
                .Where(entity => entity.DateTime.ToUniversalTime() > lastNotifiedAt.DateTime.ToUniversalTime() &&
                                 entity.DateTime.ToUniversalTime() <= timeMark).ToList();*/

            var unnotified = new List<SubscriptionEntity>();
            foreach (var entity in entities)
            {
                if (entity.DateTime.ToUniversalTime() <= lastNotifiedAt.DateTime.ToUniversalTime()) continue;
                if (entity.DateTime.ToUniversalTime() > timeMark) continue;

                unnotified.Add(entity);
            }

            return unnotified
                .Select(entity => _mapper.Map<SubscriptionModel>(entity)).ToList();
        }

        public async Task<IEnumerable<SubscriptionModel>> GetSubscripscriptions(string email, string phone)
        {
            var entities = await _subscriptionAccess.GetWithEmailAsync(email, phone);
            return entities.Select(entity => _mapper.Map<SubscriptionModel>(entity)).ToList();            
        }
    }
}
